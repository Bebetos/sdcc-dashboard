app.service("ResourceService", function ($q, $http, configService) {


    var ajax = function (method, url, data) {

        var deferred = $q.defer();
        var request = {
            method: method,
            url: url,
            headers: {
                //'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            }
        };
        if (method === 'GET' || method === 'DELETE') {
            request.params = data;
        } else {
            request.data = data;
        }
        $http(request).then(function (response) {
            deferred.resolve(response.data);
        }, function (response, status) {
            deferred.reject({data: response, status: status});
        });
        return deferred.promise;
    };

    /*** Session rest ***/
    this.login = function (params) {
        return ajax("GET", "https://" + configService.mySQL() + "/session/login", params);
    }
    this.signUp = function (params) {
        return ajax("POST", "https://" + configService.mySQL() + "/session/signup", params);
    }
    // Account
    this.changePassword = function (params) {
        return ajax("POST", "https://" + configService.mySQL() + "/profile/updatepsw", params);
    }
    this.updateInfo = function (params) {
        return ajax("POST", "https://" + configService.mySQL() + "/profile/updateinfo", params);
    }
    this.getInfo = function (params) {
        return ajax("GET", "https://" + configService.mySQL() + "/profile/getinfo", params);
    }


    /*** Statistics rest ***/
    this.getMaxValue = function (params) {
        return ajax("GET", "https://" + configService.influxDB() + "/stat/max", params);
    }
    this.getMinValue = function (params) {
        return ajax("GET", "https://" + configService.influxDB() + "/stat/min", params);
    }
    this.getAvgValue = function (params) {
        return ajax("GET", "https://" + configService.influxDB() + "/stat/avg", params);
    }
    this.getValues = function (params) {
        return ajax("GET", "https://" + configService.influxDB() + "/stat/getvalues", params);
    }
    this.getSingleValues = function (params) {
        return ajax("GET", "https://" + configService.influxDB() + "/stat/single", params);
    }
    this.getLogs = function (params) {
        return ajax("GET", "https://" + configService.influxDB() + "/stat/logs", params);
    }

    /***  Car rest***/
    this.getCars = function (params) {
        return ajax("GET", "https://" + configService.mySQL() + "/car/getall", params);
    }
    this.saveCar = function (params) {
        return ajax("POST", "https://" + configService.mySQL() + "/car/save", params);
    }
    this.deleteCar = function (params) {
        return ajax("POST", "https://" + configService.mySQL() + "/car/delete", params);
    }
    this.updateCar = function (params) {
        return ajax("POST", "https://" + configService.mySQL() + "/car/update", params);
    }

});