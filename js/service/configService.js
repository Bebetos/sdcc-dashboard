app.service("configService", function () {

    this.mySQL = function () {
        var info = [];
        info.push(config[0].mySQL);
        return info;
    }

    this.influxDB = function () {
        var info = [];
        info.push(config[1].influxDB);
        return info;
    }

});