app.controller("speedController", function ($scope, ResourceService) {

    $scope.period="day";

    $scope.getAllCars = function () {

        ResourceService.getCars({
            id: $scope.sessionID
        }).then(
            function (data) {
                result = JSON.parse(JSON.stringify(data));

                if(result.data.cars.length == 0){
                    toastr.info("Non ci sono veicoli registrati! Registrane uno", "Info");
                    $scope.container_speed = "Non ci sono veicoli registrati!";
                } else {
                    $scope.vehicles = [];
                    for (var elem in result.data.cars) {
                        var id = result.data.cars[elem].id;
                        var brand = result.data.cars[elem].brand;
                        var model = result.data.cars[elem].model;
                        var car_license_plate = result.data.cars[elem].car_license_plate;
                        var label = brand +" "+ model +" "+ car_license_plate;

                        $scope.vehicles.push({label: label, value: id});
                    }
                }
            },
            function (data) {
                //Error
                console.log(data);
            }
        )
    }

    Highcharts.setOptions({
        lang: {
            months: [
                'Gennaio', 'Febbraio', 'Marzo', 'Aprile',
                'Maggio', 'Giugno', 'Luglio', 'Agosto',
                'Settembre', 'Ottobre', 'Novembre', 'Dicembre'
            ],
            weekdays: [
                'Domenica', 'Lunedi', 'Martedi', 'Mercoledi',
                'Giovedi', 'Venerdi', 'Sabato'
            ],
            shortWeekdays: [
                'Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'
            ],
            shortMonths: [
                'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'
            ],
            noData: ['Non ci sono dati disponibili'],
            loading: ['Caricamento...']
        },
        global: {
            useUTC: false
        }
    });
    var chartSpeed = Highcharts.chart('container_speed', {
        chart: {
            zoomType: 'x'
        },
        title: {
            text: 'Velocità nel tempo'
        },
        subtitle: {
            text: ""
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: 'Velocità'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },

        series: [{
            type: 'line',
            name: 'Velocità (Km/h)',
            data: []
        }]
    });
    var chartConsumption = Highcharts.chart('container_consumption', {
        chart: {
            zoomType: 'x'
        },
        title: {
            text: 'Consumo nel tempo'
        },
        subtitle: {
            text: ""
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: 'Consumo'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },

        series: [{
            type: 'line',
            name: 'Consumo (Km/l)',
            data: []
        }]
    });


    $scope.reloadStat = function () {

        if(document.getElementById("select_vehicles").value == undefined || document.getElementById("select_vehicles").value == "?"){
            toastr.error("Veicolo selezionato non valido", "Errore");
        } else {
            $scope.drawSpeedChart();
            $scope.singleSpeedValue();
            $scope.drawConsumptionChart();
            $scope.singleConsumptionValue();
        }
    }

    // Speed stats
    $scope.singleSpeedValue = function () {
        var id_car = document.getElementById("select_vehicles").value;
        var period = document.getElementById("select_period").value;
        var stat = "speed";

        ResourceService.getSingleValues({
            id_car: id_car,
            stat: stat,
            period: period
        }).then(
            function (data) {
                result = JSON.parse(JSON.stringify(data));

                var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' , hour: 'numeric', minute: 'numeric', second : 'numeric'};

                $scope.maxSpeed = result['max'].value;
                $scope.maxTime = new Date(result['max'].time).toLocaleDateString("it-IT", options);
                $scope.minSpeed = result['min'].value;
                $scope.minTime = new Date(result['min'].time).toLocaleDateString("it-IT", options);
            },
            function (data) {
                //Error
                alert("Error Single Value!");
            }
        )
    }

    $scope.drawSpeedChart = function () {

        var id_car = document.getElementById("select_vehicles").value;
        var period = document.getElementById("select_period").value;
        var stat = "speed";

        if(id_car != undefined && period != undefined){
            ResourceService.getAvgValue({
                id_car: id_car,
                stat: stat,
                period: period
            }).then(
                function (data) {
                    // Success
                    result = JSON.parse(JSON.stringify(data));
                    var processed_json = [];
                    var i = 0;

                    if(result.length == 0){
                        chartSpeed.xAxis.min = new Date().getTime() / 1000;
                        if(period == "daily")
                            chartSpeed.xAxis.max = (new Date().getTime() / 1000) - 86400;
                        else if(period == "weekly")
                            chartSpeed.xAxis.max = (new Date().getTime() / 1000) - 604800;
                        else
                            chartSpeed.xAxis.max = (new Date().getTime() / 1000) - 2592000;
                    } else {

                        result.data.forEach(function (elem) {
                            if (elem != undefined){
                                processed_json.push([elem['time'], elem['value']]);

                                if (i == 0) {
                                    chartSpeed.xAxis.min = new Date(elem['time']);
                                }
                                i += 1;
                            }
                        });
                        chartSpeed.series[0].setData(processed_json, true);
                        chartSpeed.xAxis.max = new Date().getTime();
                        chartSpeed.redraw();
                        chartSpeed.render();
                        chartSpeed.hideLoading();
                    }
                },
                function (data) {
                    // Error
                    alert("ERROR");
                });
        }
    }


    // Consumption stat

    $scope.singleConsumptionValue = function () {
        var id_car = document.getElementById("select_vehicles").value;
        var period = document.getElementById("select_period").value;
        var stat = "consumption";

        ResourceService.getSingleValues({
            id_car: id_car,
            stat: stat,
            period: period
        }).then(
            function (data) {
                result = JSON.parse(JSON.stringify(data));

                var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' , hour: 'numeric', minute: 'numeric', second : 'numeric'};

                $scope.maxConsumption = result['max'].value;
                $scope.maxTimeConsumption = new Date(result['max'].time).toLocaleDateString("it-IT", options);
                $scope.minConsumption = result['min'].value;
                $scope.minTimeConsumption = new Date(result['min'].time).toLocaleDateString("it-IT", options);
            },
            function (data) {
                //Error
                alert("Error Single Value!");
            }
        )
    }

    $scope.drawConsumptionChart = function () {

        var id_car = document.getElementById("select_vehicles").value;
        var period = document.getElementById("select_period").value;
        var stat = "consumption";

        if(id_car != undefined && period != undefined){
            ResourceService.getAvgValue({
                id_car: id_car,
                stat: stat,
                period: period
            }).then(
                function (data) {
                    // Success
                    result = JSON.parse(JSON.stringify(data));
                    var processed_json = [];
                    var i = 0;

                    if(result.length == 0){
                        chartConsumption.xAxis.min = new Date().getTime() / 1000;
                        if(period == "daily")
                            chartConsumption.xAxis.max = (new Date().getTime() / 1000) - 86400;
                        else if(period == "weekly")
                            chartConsumption.xAxis.max = (new Date().getTime() / 1000) - 604800;
                        else
                            chartConsumption.xAxis.max = (new Date().getTime() / 1000) - 2592000;
                    } else {

                        result.data.forEach(function (elem) {
                            if (elem != undefined){
                                processed_json.push([elem['time'], elem['value']]);

                                if (i == 0) {
                                    chartConsumption.xAxis.min = new Date(elem['time']);
                                }
                                i += 1;
                            }
                        });
                        chartConsumption.series[0].setData(processed_json, true);
                        chartConsumption.xAxis.max = new Date().getTime();
                        chartConsumption.redraw();
                        chartConsumption.render();
                        chartConsumption.hideLoading();
                    }
                },
                function (data) {
                    // Error
                    alert("ERROR");
                });
        }
    }
});