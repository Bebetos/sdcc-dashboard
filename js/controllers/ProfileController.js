app.controller("profileController", function ($scope, ResourceService) {

    $scope.notMatching = false;
    $scope.btnDisable = true;

    // Checking password matching
    $scope.checkPsw = function () {

        var oldPsw = document.getElementById("psw-old").value;
        var newPsw = document.getElementById("psw-new").value;
        var newPswRep = document.getElementById("psw-new-repeat").value;

        if(newPsw.length != 0 && newPswRep.length != 0 && oldPsw.length != 0){
            if(newPsw != newPswRep){
                $scope.notMatching = true;
                $scope.btnDisable = true;
            } else {
                $scope.notMatching = false;
                $scope.btnDisable = false;
            }
        }
    }


    $scope.getInfo = function () {

        ResourceService.getInfo({
            id: $scope.sessionID,
        }).then(
            function (data) {
                $scope.name = data.name;
            },
            function (err) {
                // Error
                console.log(err);
            }
        )
    }

    $scope.savePassword = function () {

        var oldPsw = document.getElementById("psw-old").value;
        var newPsw = document.getElementById("psw-new").value;

        console.log(oldPsw);
        console.log(newPsw);

        ResourceService.changePassword({
            id: $scope.sessionID,
            old_password: oldPsw,
            password: newPsw,
        }).then(
            function (data) {
                if(data.err_code == 200){
                    toastr.success("Operazione eseguita con successo", "Password aggiornato");
                } else {
                    toastr.error("Operazione fallita", "Password non aggiornata");
                }
            },
            function (err) {
                // Error
                console.log(err);
            }
        )
    }

});