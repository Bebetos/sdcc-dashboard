app.controller("carController", function ($scope, ResourceService, $route) {


    $scope.getAllCars = function () {

        ResourceService.getCars({
            id: $scope.sessionID
        }).then(
            function (data) {
                result = JSON.parse(JSON.stringify(data));

                if(result.data.cars.length == 0){
                    toastr.info("Non ci sono veicoli registrati! Registrane uno", "Info");
                    $scope.container_speed = "Non ci sono veicoli registrati!";
                } else {
                    $scope.vehicles = [];
                    for (var elem in result.data.cars) {
                        var id = result.data.cars[elem].id;
                        var brand = result.data.cars[elem].brand;
                        var model = result.data.cars[elem].model;
                        var car_license_plate = result.data.cars[elem].car_license_plate;

                        $scope.vehicles.push({brand: brand, model: model, license_plate: car_license_plate, value: id});
                    }
                }
            },
            function (data) {
                //Error
                console.log(data);
            }
        )
    }



    // Ajax call for update car
    $scope.updateCar = function ($index) {
        var carId = $scope.vehicles[$index].value;
        var brand = document.getElementById("brand-"+$index).value;
        var model = document.getElementById("model-"+$index).value;
        var license_plate = document.getElementById("license_plate-"+$index).value;

        if (brand == '' || model == '' || license_plate == ''){
            toastr.error("Inserisci tutti i campi", "Errore");
        } else {
            ResourceService.updateCar({
                id_car: carId,
                brand: brand,
                model: model,
                car_plate: license_plate
            }).then(
                function (data) {
                    result = JSON.parse(JSON.stringify(data));
                    if(result.err_code == 200){
                        toastr.success("Operazione eseguita con successo", "Veicolo aggiornato");
                        $route.reload();
                    } else {
                        toastr.error("Operazione fallita", "Veicolo non aggiornato");
                    }
                },
                function (data) {
                    //Error
                    toastr.error("Operazione fallita", "Veicolo non aggiornato");
                    console.log(data);
                }
            )
        }

    }
    
    // Ajax call for delete a car
    $scope.deleteCar = function ($index) {

        var carId = $scope.vehicles[$index].value;

        ResourceService.deleteCar({
            id_car: carId,
        }).then(
            function (data) {
                result = JSON.parse(JSON.stringify(data));
                if(result.err_code == 200){
                    toastr.success("Operazione eseguita con successo", "Veicolo cancellato");
                    $route.reload();
                } else {
                    toastr.error("Operazione fallita", "Veicolo non cancellato");
                }
            },
            function (data) {
                //Error
                toastr.error("Operazione fallita", "Veicolo non cancellato");
                console.log(data);
            }
        )
    }


    // Ajax call for add a car
    $scope.addCar = function () {
        var brand = document.getElementById("brand").value;
        var model = document.getElementById("car_model").value;
        var license_plate = document.getElementById("car_license_plate").value;

        if (brand == '' || model == '' || license_plate == ''){
            toastr.error("Inserisci tutti i campi", "Errore");
        } else {

            ResourceService.saveCar({
                id: $scope.sessionID,
                brand: brand,
                model: model,
                car_plate: license_plate,
            }).then(
                function (data) {
                    result = JSON.parse(JSON.stringify(data));
                    if(result.err_code == 200){
                        toastr.success("Operazione eseguita con successo", "Veicolo inserito");
                        $route.reload();
                    } else {
                        toastr.error("Operazione fallita", "Veicolo non inserito");
                    }
                },
                function (data) {
                    //Error
                    toastr.error("Operazione fallita", "Veicolo non inserito");
                    console.log(data);
                }
            )

        }
    }

});
