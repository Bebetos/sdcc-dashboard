app.controller("indexController", function ($scope, configService, ResourceService) {
    
    $scope.showLogin = function () {
        $scope.login_hide = true;
        $scope.signup_hide = false;
    }

    $scope.showSignUp = function () {
        $scope.signup_hide = true;
        $scope.login_hide = false;
    }
    
    $scope.login = function () {

        var email = $scope.email_signin;
        var psw = $scope.psw_signin;

        if(email === undefined || psw === undefined){
            toastr.error("Riempire tutti i campi", "Errore");
        } else {

            ResourceService.login({
                email: email,
                password: psw
            }).then(
                function (data) {
                    if(data.err_code === 202){
                        // Utente non trovato
                        $scope.signup_hide = true;
                        $scope.login_hide = false;
                        toastr.error("Email non trovata! Registrati prima di effettuare l'accesso", "Errore");
                    } else if(data.err_code === 201){
                        // Informazioni errate
                        $scope.message = data.message;
                        $scope.signup_hide = false;
                        $scope.login_hide = true;
                        toastr.error("Email e/o password errati", "Errore");
                    } else {
                        // Success
                        $scope.sessionID = data.data.id;
                        $scope.email = email;
                        $scope.login_hide = false;
                        $scope.signup_hide = false;
                    }
                },
                function (data) {
                    // Error
                });
        }
    }
    
    $scope.signUp = function () {

        var name = $scope.name_signup;
        var email = $scope.email_signup;
        var psw = $scope.psw_signup;
        var rep_psw = $scope.repeat_psw_signup;

        if(email === undefined || psw === undefined || rep_psw === undefined || name === undefined){
            toastr.error("Riempire tutti i campi", "Errore");
        } else {

            if(psw != rep_psw){
                toastr.error("Le password non coincidono", "Errore");
            } else {

                ResourceService.signUp({
                    name: name,
                    email: email,
                    password: psw
                }).then(
                    function (data) {
                        // Success
                        $scope.message = "Sign up correctly done! please sign in."
                        $scope.login_hide = false;
                        $scope.signup_hide = true;
                        toastr.success("Esegui la procedura di login", "Successo");

                    },
                    function () {
                        // Error
                        toastr.error("Di è verificato un errore", "Errore");
                        $scope.message = "Ops! Something went wrong.";
                    });
            }
        }
    }

    $scope.logout = function () {
        $scope.sessionID = undefined;
    }

});


