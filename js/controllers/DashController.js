app.controller("dashController", function ($scope, ResourceService) {

    $scope.period="day";

    $scope.getAllCars = function () {

        ResourceService.getCars({
            id: $scope.sessionID
        }).then(
            function (data) {
                result = JSON.parse(JSON.stringify(data));

                if(result.data.cars.length == 0){
                    toastr.info("Non ci sono veicoli registrati! Registrane uno", "Info");
                    $scope.container_speed = "Non ci sono veicoli registrati!";
                } else {
                    $scope.vehicles = [];
                    for (var elem in result.data.cars) {
                        var id = result.data.cars[elem].id;
                        var brand = result.data.cars[elem].brand;
                        var model = result.data.cars[elem].model;
                        var car_license_plate = result.data.cars[elem].car_license_plate;
                        var label = brand +" "+ model +" "+ car_license_plate;

                        $scope.vehicles.push({label: label, value: id});
                    }
                }
            },
            function (data) {
                //Error
                console.log(data);
            }
        )
    }

    $scope.reloadStatDash = function () {

        if(document.getElementById("select_vehicles").value == undefined || document.getElementById("select_vehicles").value == "?"){
            toastr.error("Veicolo selezionato non valido", "Errore");
        } else {
             $scope.getAvgConsumption();
             $scope.getAvgSpeed();
             $scope.getHistoryLogs();
        }
    }
    

    $scope.getHistoryLogs = function () {

        var id_car = document.getElementById("select_vehicles").value;
        var period = document.getElementById("select_period").value;
        var stat = "log";

        $scope.lastReports = [];

        ResourceService.getLogs({
            id_car: id_car,
            stat: stat,
            period: period
        }).then(
            function (data) {

                for(var elem in data.data){
                    $scope.lastReports.push({
                        text: data.data[elem].value,
                        date:  convertDate(data.data[elem].time)
                    })
                }

                $scope.lastReports = $scope.lastReports.reverse();
            },
            function (data) {
                //Error
                alert("Error Single Value!");
            }
        )
    }


    $scope.getAvgConsumption = function () {
        var id_car = document.getElementById("select_vehicles").value;
        var period = document.getElementById("select_period").value;
        var stat = "consumption";

        ResourceService.getSingleValues({
            id_car: id_car,
            stat: stat,
            period: period
        }).then(
            function (data) {
                result = JSON.parse(JSON.stringify(data));
                $scope.avgConsumo = result['avg'].value;
            },
            function (data) {
                //Error
                alert("Error Single Value!");
            }
        )
    }


    $scope.getAvgSpeed = function () {
        var id_car = document.getElementById("select_vehicles").value;
        var period = document.getElementById("select_period").value;
        var stat = "speed";

        ResourceService.getSingleValues({
            id_car: id_car,
            stat: stat,
            period: period
        }).then(
            function (data) {
                result = JSON.parse(JSON.stringify(data));
                $scope.avgSpeedDash = result['avg'].value;
            },
            function (data) {
                //Error
                alert("Error Single Value!");
            }
        )
    }

    function pad(s) { return (s < 10) ? '0' + s : s; }

    function convertDate(timestamp) {
        var d = new Date(timestamp);
        var strDay = [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
        var strTime = [pad(d.getHours()), pad(d.getMinutes()), pad(d.getSeconds())].join(':');
        return  strDay + ' ' + strTime;
    }
});