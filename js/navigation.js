var app = angular.module("myApp", ["ngRoute", 'ngSanitize']);

app.config(function ($routeProvider) {

    $routeProvider
        .when("/", {
            templateUrl: "views/home/main.html"
        })
        .when("/stats", {
            templateUrl: "views/stats/stats.html"
        })
        .when("/profile", {
            templateUrl: "views/profile/profile.html"
        })
        .when("/car", {
            templateUrl: "views/profile/car.html"
        })
});